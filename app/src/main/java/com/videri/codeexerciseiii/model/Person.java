package com.videri.codeexerciseiii.model;

/**
 * Created by yiminglin on 6/15/17.
 */

public class Person {

    private String firstName;
    private String lastName;
    private int age;
    private String phoneNum;
    private int photoUrl;
    private boolean isLiked;

    public Person(String firstName, String lastName, Integer age, String phoneNum, int photoUrl, boolean isLiked) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.age = age;
        this.photoUrl = photoUrl;
        this.isLiked = isLiked;
    }
}
