package com.videri.codeexerciseiii.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yiminglin on 6/15/17.
 */

public class Post {
    @SerializedName("userId")
    Long userId;
    @SerializedName("id")
    Long id;
    @SerializedName("title")
    String title;
    @SerializedName("body")
    String body;
}
