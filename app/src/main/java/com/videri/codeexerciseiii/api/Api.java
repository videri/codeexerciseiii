package com.videri.codeexerciseiii.api;

import com.videri.codeexerciseiii.model.Post;

import java.util.List;

import retrofit2.Callback;
import retrofit2.http.GET;

/**
 * Created by yiminglin on 6/15/17.
 */

public interface Api {
    @GET("/movie/posts")
    void getPopularMovies(
            Callback<List<Post>> callback);
}
