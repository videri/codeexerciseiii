package com.videri.codeexerciseiii.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.videri.codeexerciseiii.Constants;

import retrofit2.Retrofit;

/**
 * Created by yiminglin on 6/15/17.
 */

public class RestClient {


    private final Api apis;

    public RestClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.HOST)
                .build();
        apis = retrofit.create(Api.class);
    }
}
