package com.videri.codeexerciseiii;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.videri.codeexerciseiii.model.Person;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * The goal of this exercise is to run a functional application that is architected well. It should show a list of {@link Person} items,
 * and based on user input, modifying the item. After completing, show the result on the emulator.
 * Feel free to consult web sites/ personal projects etc.
 *
 * Part I. Show a list of people and details.
 * - Show name, a heart icon, and an avatar image as a list as part of the MainActivity.
 * - When clicking on an item, all information is displayed on DetailActivity with the ability “Like” an item.
 * - In addition, create a “Like” button on the detail page - this should change the drawable image from “not liked” to “Liked”  on the main list in MainActivity.
 *
 * - There are screen shots and video examples for how the layouts are supposed to look in the root directory.
 *
 *
 * Part II. show a list of items as a GET API Request to server using any kind of framework you know.{@link APIActivity}
 *
 *
 * Part III. Discussion: (Explanation in words)
 * - Discuss how you would display and use the information retrieved from the below endpoint.
 *      http://jsonplaceholder.typicode.com/users
 * - Discuss how to handle network errors.
 * - Discuss how to model the object.
 *
 *
 * WE PREFER FUNCTIONALITY FIRST, LAYOUT LAST
 *
 * <p/>
 * bonus points for:
 *  adding any test cases, frameworks, and code relevant.
 *  using MVP structure.
 *  suggesting/using up-to-date animation and transition effects.
 *  using third party libraries.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void initPerson() {
        final List<Person> persons = new ArrayList<>();
        persons.add(new Person("Kayleigh", "Kettering", 5, "111-222-3337", R.drawable.female_fa_1_icon, false));
        persons.add(new Person("Kurt", "Pauling", 5, "111-222-3337", R.drawable.female_face_fc_1_icon, false));
        persons.add(new Person("Magan", "Cambell", 5, "111-222-3337", R.drawable.female_face_fd_1_dark_icon, false));
        persons.add(new Person("Jamar", "Mallen", 5, "111-222-3337", R.drawable.female_face_fe_1_blonde_icon, false));
        persons.add(new Person("Brooks", "Pangborn", 20, "111-222-3333", R.drawable.male_face_a1_icon, false));
        persons.add(new Person("Dexter", "Skelly", 30, "111-222-3334",R.drawable.male_face_b1_icon , false));
        persons.add(new Person("Merlin", "Peterka", 24, "111-222-3335", R.drawable.male_face_c1_icon, false));
        persons.add(new Person("Silas", "Galentine", 15, "111-222-3336", R.drawable.male_face_g1_icon, false));
        persons.add(new Person("Kurtis", "Lineman", 5, "111-222-3337", R.drawable.male_face_e1_icon, false));
    }
}
